 import java.util.Scanner;
class Main {
  public static void main(String[] args) {
    Scanner entrada = new Scanner(System.in);
    double cl, ope;
    double numero;
    int conversion;
  
    System.out.println("Digite la cantidad de lanzamientos");
    cl = entrada.nextInt();

    System.out.println("Digite los valores");
    int numeros [] = new int[(int)cl];

    for(int i=0; i< cl;i++){
      numero=entrada.nextFloat();
      ope=6*numero;
      conversion = (int)ope+1;
      numeros[i]=conversion;
    }

    for(int i:numeros){
      System.out.println(i);
    }
  }
}